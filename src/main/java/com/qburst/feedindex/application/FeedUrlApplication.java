package com.qburst.feedindex.application;

import com.qburst.feedindex.controllers.FeedUrlController;
import com.qburst.feedindex.services.RedisConnector;
import org.apache.flume.Context;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.conf.Configurable;
import org.apache.flume.source.AbstractSource;
import org.apache.log4j.Logger;

import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.*;

public class FeedUrlApplication extends AbstractSource implements EventDrivenSource, Configurable {
    private static Context context;
    final static Logger logger = Logger.getLogger("FeedIndex");

    @Override
    public void start() {
        final RedisConnector redisConn = new RedisConnector(context);
        final Set<String> urlList = redisConn.getLinks();
        int listCount = 0;

        super.start();

        for (String url : urlList) {
            logger.info("======> Processing " + url);
            redisConn.removeLink(url);

            BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<>(500);
            ThreadPoolExecutor executor = new ThreadPoolExecutor(20, 25, 1000, TimeUnit.MILLISECONDS, blockingQueue);
            executor.setRejectedExecutionHandler(new RejectedExecutionHandler() {
                @Override
                public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        logger.warn(e.getMessage());
                    }
                    executor.execute(r);
                }
            });

            FeedUrlController.setExecutor(executor);
            FeedUrlController.getExecutor().prestartAllCoreThreads();
            FeedUrlController.getFeedList().clear();

            int count = 20;
            TreeSet<String> links = FeedUrlController.getFeedUrl(url, 2);

            while (!(executor.getActiveCount() == 0 || links.size() >= 100)) {
            }
            executor.shutdownNow();

            if (!links.isEmpty()) {
                logger.info("Count for " + url + " = " + links.size());
                if (links.size() < 50) {
                    count = 10;
                }
                synchronized (links) {
                    for (String link : links) {
                        if (redisConn.insertLink(link, 3)) {
                            logger.info(link);
                        }
                        if (--count == 0) {
                            break;
                        }
                    }
                }
            } else {
                logger.warn("No Feed URL found.");
            }
            logger.info("======> " + (urlList.size() - (++listCount)) + " links remaining");

            try {
                while (!executor.awaitTermination(250, TimeUnit.MILLISECONDS)) {
                }
            } catch (InterruptedException e) {
                logger.warn(e.getMessage());
            }
        }

        try {
            logger.info("FeedURL process completed, sleeping for 45 minutes.");
            redisConn.destroyPool();
            TimeUnit.MINUTES.sleep(45);
            start();
        } catch (InterruptedException e) {
            logger.warn(e.getMessage());
        }
    }

    @Override
    public void configure(Context context) {
        FeedUrlApplication.context = context;
    }

    @Override
    public void stop() {
        super.stop();
        logger.info("FeedURL terminated.");
    }
}

