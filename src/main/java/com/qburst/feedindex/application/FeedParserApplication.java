package com.qburst.feedindex.application;

import com.qburst.feedindex.models.FeedUrlModel;
import com.qburst.feedindex.services.ParseThread;
import com.qburst.feedindex.services.RedisConnector;
import org.apache.flume.Context;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.channel.ChannelProcessor;
import org.apache.flume.conf.Configurable;
import org.apache.flume.source.AbstractSource;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

public class FeedParserApplication extends AbstractSource implements EventDrivenSource, Configurable {

    private Context context;
    private static int maxCoreThread = 10;
    private static int maxPoolSize = 25;
    static Logger logger = Logger.getLogger("FeedParser");

    public static ChannelProcessor getChannel() {
        return channel;
    }

    private static ChannelProcessor channel = null;


    @Override
    public void start() {

        logger.info("*** Source Started");
        channel = getChannelProcessor();
        ArrayList<FeedUrlModel> feedList = new ArrayList<>();
        ArrayList<ParseThread> threadList = new ArrayList<>();

        BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<>(500);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(maxCoreThread, maxPoolSize, 50, TimeUnit.MICROSECONDS, blockingQueue);
        executor.setRejectedExecutionHandler(new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r,
                                          ThreadPoolExecutor executor) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                }
                executor.execute(r);
            }
        });

        logger.info("Redis Connnecting ..");
        final RedisConnector conn = new RedisConnector(context);
        logger.info("Redis Connnected");
        FeedUrlModel feed = null;
        Set<String> links = conn.getFeeds();
        logger.info("*** " + links.size() + " feeds are present in feedset ");
        for (String link : links) {
            Map<String, String> linkvalue = conn.getFeed(link);
            feed = new FeedUrlModel(link, linkvalue.get("time"));
            feedList.add(feed);
            ParseThread parseThread = new ParseThread(feed.getUrl(), feed.getTime());
            threadList.add(parseThread);
            executor.execute(parseThread);
        }

        executor.shutdown();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (executor.getActiveCount() > 0) ;
        for (ParseThread parseThread2 : threadList) {
            if (parseThread2.isMalformed()) {
                logger.info("Removing malformed rsslink " + parseThread2.getRsslink());
                conn.removeFeed(parseThread2.getRsslink());
            } else if (parseThread2.getLastPub() == null) {
                conn.removeFeed(parseThread2.getRsslink());
            } else {
                conn.updateTime(parseThread2.getRsslink(), parseThread2.getLastPub());
            }
        }
        logger.info("Updation to Database completed");
        logger.info("Program Exiting Now");
        stop();
    }


    public void configure(Context context) {

        this.context = context;

    }

    @Override
    public void stop() {
        super.stop();
    }
}