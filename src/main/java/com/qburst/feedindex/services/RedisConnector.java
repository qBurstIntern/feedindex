package com.qburst.feedindex.services;

import org.apache.flume.Context;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.util.Date;
import java.util.Map;
import java.util.Set;

public class RedisConnector {
    final Logger logger = Logger.getLogger("RedisConnector");
    private static JedisPool pool;
    private static String urlset;
    private static String feedset;

    public RedisConnector(Context context) {
        urlset = context.getString("urlset");
        feedset = context.getString("feedset");
        pool = new JedisPool(new JedisPoolConfig(), context.getString("host"), context.getInteger("port"), 2000, context.getString("password"));
    }

    public Set<String> getLinks() {
        try (Jedis jedis = pool.getResource()) {
            return jedis.smembers(urlset);
        }
    }

    public void removeFeed(String url) {
        try (Jedis jedis = pool.getResource()) {
            jedis.del(url);
            jedis.srem(feedset, url);
        }
    }

    public void removeLink(String url) {
        try (Jedis jedis = pool.getResource()) {
            jedis.srem(urlset, url);
        }
    }

    public void updateTime(String url, Date lastPub) {
        try (Jedis jedis = pool.getResource()) {
            if (lastPub != null) {
                jedis.hset(url, "time", String.valueOf(lastPub.getTime()));
            } else {
                System.out.println(":: " + url + "  given no lastPub");
                jedis.hset(url, "time", "");
            }
        }
    }

    public boolean insertLink(String link, int level) {
        try (Jedis jedis = pool.getResource()) {
            if (!jedis.exists(link)) {
                jedis.hset(link, "time", "");
                jedis.sadd(feedset, link);
            }
            return true;
        } catch (JedisConnectionException e) {
            logger.warn(e.getMessage());
            if (level > 0) {
                logger.warn("Retrying, " + link);
                return insertLink(link, level - 1);
            } else {
                return false;
            }
        }
    }

    public Set<String> getFeeds() {
        try (Jedis jedis = pool.getResource()) {
            return jedis.smembers(feedset);
        }
    }

    public Map<String, String> getFeed(String link) {
        try (Jedis jedis = pool.getResource()) {
            return jedis.hgetAll(link);
        }
    }

    public void destroyPool() {
        pool.destroy();
    }

    protected void finalize() {
        pool.destroy();
    }

}