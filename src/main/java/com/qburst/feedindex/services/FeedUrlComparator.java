package com.qburst.feedindex.services;

import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;

public class FeedUrlComparator implements Comparator<String> {
    public int compare(String s1, String s2) {
        if (s1.equals(s2)) {
            return 0;
        }
        if (StringUtils.countMatches(s1, "/") < StringUtils.countMatches(s2, "/")) {
            return -1;
        } else if (StringUtils.countMatches(s1, "/") == StringUtils.countMatches(s2, "/")) {
            if (s1.length() <= s2.length()) {
                return -1;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }
}
