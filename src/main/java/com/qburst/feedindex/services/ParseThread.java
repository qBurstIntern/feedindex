package com.qburst.feedindex.services;

import com.qburst.feedindex.controllers.FeedParserController;
import com.qburst.feedindex.models.FeedModel;
import org.apache.log4j.Logger;

import java.util.Date;

public class ParseThread extends Thread {

    private String rsslink;
    private Date time;
    private Logger logger;
    private Date lastPub;
    private boolean malformed;

    public ParseThread(String rsslink, Date time) {
        this.rsslink = rsslink;
        this.time = time;
        logger = Logger.getLogger("FeedIndex");
    }

    public String getRsslink() {
        return rsslink;
    }

    public boolean isMalformed() {
        return malformed;
    }

    public void run() {
        FeedParserController parser = new FeedParserController(rsslink, time);
        FeedModel feed = parser.getFeed();
        lastPub = feed.getPubDate();
        if (lastPub == null) {
            try {
                lastPub = feed.getMessages().get(0).getPubDate();
            } catch (NullPointerException e) {
                lastPub = null;
            } catch (IndexOutOfBoundsException e) {
                lastPub = null;
            }
        }
        malformed = parser.isMalformed();
        logger.info("Total Articles  : " + feed.getMessages().size());
    }

    public Date getLastPub() {
        return lastPub;
    }
}
