package com.qburst.feedindex.services;

import com.qburst.feedindex.controllers.FeedUrlController;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.TreeSet;

public class FeedUrlProcessor implements Runnable {
    private static Logger logger = Logger.getLogger("FeedUrlThread");
    Document doc;
    private TreeSet<String> feedList;
    private Element link;

    public FeedUrlProcessor(TreeSet<String> feedList, Element link) {
        this.link = link;
        this.feedList = feedList;
    }

    @Override
    public void run() {
        try {
            String linkStr = link.attr("abs:href");
            if (!feedList.contains(linkStr)) {
                doc = Jsoup.connect(linkStr).get();
                if (!doc.select("rss,feed").isEmpty()) {
                    logger.info(linkStr);
                    synchronized (feedList) {
                        feedList.add(linkStr);
                    }
                    FeedUrlController.setFlag(true);
                }
            } else {
                FeedUrlController.setFlag(true);
            }
        } catch (Exception e) {
            logger.warn(e.getMessage());
        }
    }
}
