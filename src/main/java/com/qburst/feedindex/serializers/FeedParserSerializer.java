package com.qburst.feedindex.serializers;

import com.google.common.collect.Lists;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.conf.ComponentConfiguration;
import org.apache.log4j.Logger;
import org.hbase.async.AtomicIncrementRequest;
import org.hbase.async.PutRequest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class FeedParserSerializer implements org.apache.flume.sink.hbase.AsyncHbaseEventSerializer {

    private static Logger logger = Logger.getLogger(FeedParserSerializer.class);

    private byte[] table;
    private static byte[] DATACOLUMN_FAMILY;
    private static byte[] METADATACOLUMN_FAMILY = "metadata".getBytes();

    private static Timestamp currentTimestamp;
    private static Timestamp previousTimestamp;
    private static final String STRUCTURED_TABLE = "structured_data";
    private static final String UNSTRUCTURED_TABLE = "unstructured_data";

    private Event currentEvent;

    private static final String DATA_HEADER = "data";
    private static final String ROWKEY_HEADER = "rowKey";

    private static final String LINKCOLUMN_HEADER = "link";
    private static final String DATATYPE_HEADER = "datatype";
    private static final String STRUCTURED_HEADER = "isstructured";
    private static final String CELEBRITY_HEADER = "celebrity";


    private final ArrayList<PutRequest> putRequests = Lists.newArrayList();
    private byte[] currentRow;
    private String datatype;
    private boolean shouldProcess = false;
    private Boolean isStrucutred = null;
    private String link;
    private String celebrity;
    private final List<AtomicIncrementRequest> incs = new ArrayList<>();

    private final byte[] eventCountCol = "eventCount".getBytes();


    @Override
    public void initialize(byte[] table, byte[] cf) {
        logger.info("*** Serializer initilized");
        this.table = table;
        this.DATACOLUMN_FAMILY = cf;
    }

    @Override
    public void setEvent(Event event) {
        this.currentEvent = event;

        Map<String, String> headers = currentEvent.getHeaders();

        currentTimestamp = new Timestamp(new Date().getTime());
        if (previousTimestamp == currentTimestamp) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentTimestamp = new Timestamp(new Date().getTime());
        }
        previousTimestamp = currentTimestamp;

        isStrucutred = Boolean.parseBoolean(headers.get(STRUCTURED_HEADER));
        datatype = headers.get(DATATYPE_HEADER);
        link = headers.get(LINKCOLUMN_HEADER);
        if (isStrucutred) {
            celebrity = headers.get(CELEBRITY_HEADER);
            table = STRUCTURED_TABLE.getBytes();
        } else {
            table = UNSTRUCTURED_TABLE.getBytes();
        }
        if (datatype == null || link == null || isStrucutred == null) {
            shouldProcess = false;
            return;
        }
        StringBuffer rowKey = new StringBuffer(Long.toString(currentTimestamp.getTime()));
        currentRow = rowKey.reverse().toString().getBytes();
        shouldProcess = true;

    }

    @Override
    public List<PutRequest> getActions() {

        putRequests.clear();
        if (shouldProcess) {
            putRequests.add(new PutRequest(table, currentRow, DATACOLUMN_FAMILY,
                    DATA_HEADER.getBytes(), currentEvent.getBody()));
            putRequests.add(new PutRequest(table, currentRow, METADATACOLUMN_FAMILY,
                    DATATYPE_HEADER.getBytes(), datatype.getBytes()));
            putRequests.add(new PutRequest(table, currentRow, METADATACOLUMN_FAMILY,
                    LINKCOLUMN_HEADER.getBytes(), link.getBytes()));
            if (isStrucutred) {
                putRequests.add(new PutRequest(table, currentRow, METADATACOLUMN_FAMILY,
                        CELEBRITY_HEADER.getBytes(), celebrity.getBytes()));
            }
        }
        return putRequests;
    }

    @Override
    public List<AtomicIncrementRequest> getIncrements() {
        incs.clear();
        //Increment the number of events received
        incs.add(new AtomicIncrementRequest(table, "totalEvents".getBytes(), METADATACOLUMN_FAMILY, eventCountCol));
        return incs;
    }

    @Override
    public void cleanUp() {
        putRequests.clear();
        currentEvent = null;
        currentRow = null;
        table = null;
    }


    @Override
    public void configure(Context context) {

    }

    @Override
    public void configure(ComponentConfiguration componentConfiguration) {

    }
}
