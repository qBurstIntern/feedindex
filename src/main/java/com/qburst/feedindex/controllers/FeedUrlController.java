package com.qburst.feedindex.controllers;

import com.qburst.feedindex.services.FeedUrlComparator;
import com.qburst.feedindex.services.FeedUrlProcessor;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.TreeSet;
import java.util.concurrent.ThreadPoolExecutor;

public class FeedUrlController {
    private static Logger logger = Logger.getLogger("FeedUrlController");
    private static TreeSet<String> feedList = new TreeSet<>(new FeedUrlComparator());
    private static ThreadPoolExecutor executor;
    private static boolean flag;

    public static TreeSet<String> getFeedList() {
        return feedList;
    }

    public static ThreadPoolExecutor getExecutor() {
        return executor;
    }

    public static void setExecutor(ThreadPoolExecutor executor) {
        FeedUrlController.executor = executor;
    }

    public static void setFlag(boolean flag) {
        FeedUrlController.flag = flag;
    }

    public static TreeSet<String> getFeedUrl(String url, int level) {
        if (level != 0) {
            Document doc;
            flag = false;

            try {
                doc = Jsoup.connect(url).get();
                Elements links = doc.select("a[href*=rss],a[href*=feed],a[href*=xml]");

                for (Element link : links) {
                    FeedUrlProcessor processor = new FeedUrlProcessor(feedList, link);
                    Thread thread = new Thread(processor);
                    executor.execute(thread);
                }

                if (!flag && feedList.size() < 100) {
                    for (Element link : links) {
                        getFeedUrl(link.attr("abs:href"), level - 1);
                        if (feedList.size() >= 100) {
                            return feedList;
                        }
                    }
                }
            } catch (Exception e) {
                logger.warn(e.getMessage());
            }
        }
        return feedList;
    }
}
