package com.qburst.feedindex.controllers;


import com.qburst.feedindex.application.FeedParserApplication;
import com.qburst.feedindex.models.FeedMessageModel;
import com.qburst.feedindex.models.FeedModel;
import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import org.apache.flume.Event;
import org.apache.flume.event.EventBuilder;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class FeedParserController {

    private static Logger logger = Logger.getLogger(FeedParserController.class);
    private String url;
    private FeedModel feedmodel;
    private Date lastPublished;
    private boolean malformed;

    static private Map<String, String> headers = new HashMap<String, String>();

    public FeedParserController(String url, Date lastPublished) {
        this.url = url;
        this.lastPublished = lastPublished;
        malformed = false;
        parseRSS();
        storeDetails();
    }

    public boolean isMalformed() {
        return malformed;
    }

    public void parseRSS() {

        SyndFeed feed;
        InputStream is = null;

        try {
            URLConnection openConnection = new URL(url).openConnection();
            is = new URL(url).openConnection().getInputStream();
            if ("gzip".equals(openConnection.getContentEncoding())) {
                is = new GZIPInputStream(is);
            }
            InputSource source = new InputSource(is);
            SyndFeedInput input = new SyndFeedInput();
            feed = input.build(source);

            feedmodel = new FeedModel();
            feedmodel.setFeedUrl(feed.getLink());
            if (feed.getPublishedDate() != null) {
                feedmodel.setPubDate(feed.getPublishedDate());
            }

            for (Iterator i = feed.getEntries().iterator(); i.hasNext(); ) {
                SyndEntry entry = (SyndEntry) i.next();
                FeedMessageModel message = new FeedMessageModel();
                message.setTitle(entry.getTitle());
                message.setUrl(entry.getLink());

                Document xmlDoc = Jsoup.parse(entry.getDescription().getValue(), "", Parser.xmlParser());
                String parsedDescription = xmlDoc.select("*").get(0).text().trim();
                if (parsedDescription == null) {
                    parsedDescription = "";
                }

                for (Iterator contentIterator = entry.getContents().iterator(); contentIterator.hasNext(); ) {
                    SyndContent sc = (SyndContent) contentIterator.next();
                    xmlDoc = Jsoup.parse(sc.getValue(), "", Parser.xmlParser());
                    String parsedContent = xmlDoc.select("*").get(0).text().trim();
                    if (parsedContent != null) {
                        parsedDescription += parsedContent;
                    }
                }
                message.setDescription(parsedDescription);
                message.setPubDate(entry.getPublishedDate());
                feedmodel.addFeedMessage(message);
            }
        } catch (MalformedURLException e) {
            malformed = true;
            logger.info(e.getMessage());
        } catch (FeedException e) {
            malformed = true;
            logger.info(e.getMessage());
        } catch (IOException e) {
            logger.info(e.getMessage());
        } catch (NullPointerException e) {
            logger.info(e.getMessage());
        } catch (Exception e) {
            logger.info(e.getMessage());
            malformed = true;
        } finally {
            if (is != null) try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public FeedModel getFeed() {
        return feedmodel;
    }

    public void storeDetails() {

        for (FeedMessageModel message : feedmodel.getMessages()) {
            if (message.getPubDate() != null) {
                if (lastPublished == null || message.getPubDate().after(lastPublished)) {
                    String[] entries = {message.getPubDate().toString(), message.getTitle(), message.getDescription(), message.getUrl()};
                    Event event = null;
                    headers.put("datatype", "rss");
                    headers.put("link", message.getUrl());
                    headers.put("isstructured", "false");
                    event = EventBuilder.withBody((message.getTitle() + message.getDescription()).trim().getBytes(), headers);
                    FeedParserApplication.getChannel().processEvent(event);
                } else {
                    break;
                }
            }

        }
    }
}