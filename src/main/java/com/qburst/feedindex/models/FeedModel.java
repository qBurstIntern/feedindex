package com.qburst.feedindex.models;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FeedModel {

    private String feedUrl;
    private Date pubDate;

    private List<FeedMessageModel> messages = new ArrayList<FeedMessageModel>();

    public String getFeedUrl() {
        return feedUrl;
    }

    public void setFeedUrl(String feedUrl) {
        this.feedUrl = feedUrl;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public void addFeedMessage(FeedMessageModel feedmessage) {
        messages.add(feedmessage);
    }

    public List<FeedMessageModel> getMessages() {
        return messages;
    }

    public String toString() {
        return "feedurl :: " + feedUrl + " pubdate:: " + pubDate;
    }
}
