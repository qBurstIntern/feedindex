package com.qburst.feedindex.models;

import java.util.Date;

public class FeedUrlModel {
    private String url;
    private Date time;

    public FeedUrlModel() {
    }

    public FeedUrlModel(String url) {
        this.url = url;
    }

    public FeedUrlModel(String url, Date date) {
        this.url = url;
        this.time = date;
    }

    public FeedUrlModel(String url, String date) {
        this.url = url;
        if (date != null && !date.equals("")) {
            this.time = new Date(Long.valueOf(date).longValue());
        }
        this.time = null;

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
