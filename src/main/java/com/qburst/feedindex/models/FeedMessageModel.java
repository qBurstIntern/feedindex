package com.qburst.feedindex.models;


import java.util.Date;

public class FeedMessageModel {
    private String url;
    private String title;
    private String description;
    private Date pubDate;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (title == null) {
            this.title = "";
        } else {
            this.title = title;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description == null) {
            this.description = "";
        } else {
            this.description = description;
        }
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String toString() {

        return "\nurl::  " + url + "\n\t title:: " + title + " \n\tdescription:: " + description + "\n\tpubDate:: " + pubDate;
    }
}
